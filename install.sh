#!/bin/bash

set -o errexit  # fail on simple (non-piped) error
set -o pipefail # also fail on piped commands (e.g. cat myfile.txt | grep timo)
set -o nounset  # fail when accessing unset vars

############################
# .make.sh
# This script creates symlinks from the home directory to any desired dotfiles in ~/dotfiles
# copied from http://blog.smalleycreative.com/tutorials/using-git-and-github-to-manage-your-dotfiles/
# thx!!!
############################

########## Variables

dir=~/programming/codeberg.org/wolframkriesing/dotfiles # dotfiles directory
olddir=~/dotfiles_old               # old dotfiles backup directory
# list of files/folders to symlink in homedir
files="bashrc vim vimrc tmux.conf gitignore gitconfig dockerfunc"

##########

# create dotfiles_old in homedir
echo "Create backup dir: $olddir"
mkdir -p $olddir

# change to the dotfiles directory
echo "Change to: $dir"
cd $dir

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks
for file in $files; do
    if [ -L "${file}" ] ; then
      echo "Move previous existing $file from ~ to $olddir"
      mv ~/."$file" ~/dotfiles_old/
    fi
    echo "Creating symlink to .$file in home directory."
    ln -sf $dir/$file ~/.$file
done
