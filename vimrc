execute pathogen#infect()
filetype plugin indent on

syntax on
set number
set ruler
set showcmd
set noswapfile

colorscheme pablo
" make comment color more readable
highlight Comment guifg=LightGrey ctermfg=LightGrey
highlight String guibg=DarkBlue ctermbg=DarkBlue
